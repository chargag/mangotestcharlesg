﻿import React, { Component } from 'react';
import { Modal, Button, Form } from "react-bootstrap"

export class ItemModal extends Component {

    constructor(props) {
        super(props);
        this.state = { title: "", description: "" };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        let fieldName = event.target.name;
        let fleldVal = event.target.value;
        this.setState({ [fieldName]: fleldVal})
    }

    render() {
        const { title, description } = this.state;
        return (
            <Modal show={true} onHide={this.props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Ajouter un item</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group>
                            <Form.Label>Titre :</Form.Label>
                            <Form.Control type="text" name={"title"} onChange={this.handleChange} value={title} placeholder={"Écrire un titre"} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Description :</Form.Label>
                            <Form.Control type="text" name={"description"} onChange={this.handleChange} value={description} placeholder={"Écrire une description"} />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.props.handleClose}>
                        Close
                     </Button>
                    <Button variant="primary" onClick={() => this.props.handleClose(this.state)}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}
