import React, { Component } from 'react';
import authService from './api-authorization/AuthorizeService'
import { ItemModal } from './ItemForm';
import {Button} from "react-bootstrap"


export class Items extends Component {
  static displayName = Items.name;

  constructor(props) {
    super(props);
      this.state = { items: [], loading: true, show: false };
      this.handleClose = this.handleClose.bind(this);
  }

  componentDidMount() {
    this.populateTableData();
  }

  static renderItemsTable(items) {
    return (
      <table className='table table-striped' aria-labelledby="tabelLabel">
        <thead>
          <tr>
            <th>#</th>
            <th>Nom</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          {items.map((item, i) =>
              <tr key={i + 1}>
                  <td>{i + 1}</td>
              <td>{item.itemName}</td>
              <td>{item.itemDescription}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
    }

    handleClose(formValues) {
        this.setState({ show: false });
        if (formValues) {
            this.addItem(formValues).then(this.populateTableData.bind(this));
        }
    }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
      : Items.renderItemsTable(this.state.items);

    return (
      <div>
        <h1 id="tabelLabel" >Items disponibles</h1>
            {contents}
            <Button onClick={() => this.setState({show: true})}>Ajouter</Button>
            {this.state.show ? <ItemModal handleClose={this.handleClose} /> : null}
      </div>
    );
  }

    async addItem(item) {
        const token = await authService.getAccessToken();
        await fetch('/Items', {
            headers: !token ? {} : { 'Authorization': `Bearer ${token}`, 'Content-Type': 'application/json' },
            method: "post",
            body: JSON.stringify(item)
        });
    }

  async populateTableData() {
    const token = await authService.getAccessToken();
    const response = await fetch('/Items', {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
      const data = await response.json();
    this.setState({ items: data, loading: false });
  }
}
