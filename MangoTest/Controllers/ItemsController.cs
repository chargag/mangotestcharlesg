﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MangoTest.Data;
using MangoTest.Data.Dao;
using MangoTest.Data.Repos;
using MangoTest.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MangoTest.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class ItemsController : ControllerBase
    {
        private readonly IItemsRepo _itemsContext;

        public ItemsController(IItemsRepo itemsContext)
        {
            _itemsContext = itemsContext;
        }

        [HttpGet]
        public IEnumerable<Item> Get()
        {
            return _itemsContext.GetAllItems();
        }

        [HttpPost]
        public void Add([FromBody] Dictionary<string, string> data)
        {
            _itemsContext.AddItem(data["title"], data["description"]);
        }
    }
}
