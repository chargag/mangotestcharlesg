﻿using MangoTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MangoTest.Data.DataSeeder;


namespace MangoTest.Data.DataSeeder
{
    public static class ItemSeeder
    {
        public static void SeedItems(ApplicationDbContext context)
        {
            if (!context.Items.Any())
            {
                List<Item> items = new List<Item>() { };
                for (int i = 0; i < 10; i++)
                {
                    items.Add(new Item($"Item {i + 1}", $"Description de l'item {i + 1}"));
                }
                context.Items.AddRange(items);
                context.SaveChanges();
            }
        }
    }
}
