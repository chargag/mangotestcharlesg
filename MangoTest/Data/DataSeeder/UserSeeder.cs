﻿using MangoTest.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MangoTest.Data.DataSeeder
{
    public static class UserSeeder
    {
        public static void SeedData(UserManager<ApplicationUser> userManager)
        {
            if (userManager.FindByEmailAsync("user@test.com").Result == null)
            {
                ApplicationUser user = new ApplicationUser();
                user.UserName = "user@test.com";
                user.Email = "user@test.com";


                IdentityResult result = userManager.CreateAsync(user, "P@ssw0rd1!").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "User").Wait();
                }
            }
        }
    }
}
