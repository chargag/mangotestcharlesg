﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MangoTest.Data.Migrations
{
    public partial class removedcolumn2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ItemQty",
                table: "Items");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ItemQty",
                table: "Items",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
