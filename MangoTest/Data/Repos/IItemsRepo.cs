﻿using MangoTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MangoTest.Data.Repos
{
    public interface IItemsRepo
    {
        IEnumerable<Item> GetAllItems();
        void AddItem(string title, string description);
    }
}
