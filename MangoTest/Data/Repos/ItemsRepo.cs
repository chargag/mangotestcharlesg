﻿using MangoTest.Data.Repos;
using MangoTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MangoTest.Data.Dao
{
    public class ItemsRepo : IItemsRepo
    {
        private readonly ApplicationDbContext _context;

        public ItemsRepo(ApplicationDbContext ctx)
        {
            _context = ctx;
        }

        public void AddItem(string title, string description)
        {
            _context.Items.Add(new Item(title, description));
            _context.SaveChanges();
        }

        public IEnumerable<Item> GetAllItems()
        {
            return _context.Items.ToList();
        }
    }
}
