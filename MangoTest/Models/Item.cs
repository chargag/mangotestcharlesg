﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MangoTest.Models
{
    public class Item
    {
        public Item(string ItemName, string ItemDescription) {
            this.ItemName = ItemName;
            this.ItemDescription = ItemDescription;
        }

        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
    }
}
